// using System;
// using System.Data;
// using System.Data.SQLite;

// namespace Playlister_Class_Library
// {
//     public class Music_DataTables
//     {
//         private DataTable song;
//         private DataTable genre;
//         private DataTable artist;

//         public DataTable Song_Table { get => song; }

//         public DataTable Genre_Table { get => genre; }
//         public DataTable Artist_Table { get => artist; }
//         public const string Database_Connection_String = " Data source = music_database.db";
//         public readonly Music_DataTable_Names Table_Names = new Music_DataTable_Names("song", "genre", "artist");

//         public struct Music_DataTable_Names
//         {
//             private string song;
//             private string genre;
//             private string artist;

//             public Music_DataTable_Names(string new_song_Table_name, string new_Genre_Table_name, string new_Artist_Table_name)
//             {
//                 song = new_song_Table_name;
//                 genre = new_Genre_Table_name;
//                 artist = new_Artist_Table_name;
//             }
//         }

//         public Music_DataTables()
//         {
//             SQLiteConnection song_Metadata_Connection = new SQLiteConnection(Database_Connection_String);
//             song_Metadata_Connection.Open();

//             var Sql_Transaction = song_Metadata_Connection.BeginTransaction();
//             try
//             {
//                 SQLiteCommand get_Table_Metadata = new SQLiteCommand("SELECT * from song", song_Metadata_Connection);
//                 var song_Table_Datareader = get_Table_Metadata.ExecuteReader();
//                 song.Load(song_Table_Datareader);
//                 get_Table_Metadata.CommandText = "SELECT * from genre";
//                 var genre_Table_Datareader = get_Table_Metadata.ExecuteReader();
//                 genre.Load(genre_Table_Datareader);

//                 get_Table_Metadata.CommandText = "SELECT * from artist";
//                 var artist_Table_Datareader = get_Table_Metadata.ExecuteReader();
//                 artist.Load(artist_Table_Datareader);

//                 Sql_Transaction.Commit();
//             }
//             catch (Exception e)
//             {
//                 Sql_Transaction.Rollback();
//                 Console.WriteLine("could not connect to database");
//                 Console.WriteLine(e.ToString());
//             }
//         }

//         public void Save_Song_Datatable()
//         {
//             SQLiteConnection song_Metadata_Connection = new SQLiteConnection(Database_Connection_String);
//             song_Metadata_Connection.Open();
//             SQLiteDataAdapter adapter = new SQLiteDataAdapter();
//             SQLiteCommandBuilder command_builder = new SQLiteCommandBuilder(adapter);
//             adapter.UpdateCommand = command_builder.GetUpdateCommand();
//             adapter.Update(song);
//         }
//     }
// }