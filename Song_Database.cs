﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Playlister_Class_Library
{
    public class Song_Database
    {
        private string database_Name;


        private IMusic_Context music_Context;

        public Song_Database(string New_Database_Name, IMusic_Context context)
        {
            database_Name = New_Database_Name;
            music_Context = context;
        }

        public Song_Database(string New_Database_Name)
        {
            database_Name = New_Database_Name;
        }

        /// <summary>
        /// adds a song to the music index checking if the song is already contained in the collection
        /// </summary>
        /// <param name="New_Song"> the song object to be added </param>
        /// <returns>if the song is successfully inputed into the index </returns>
        public void Add_Songs(Song[] New_Songs)
        {
            var currentHashSet = new HashSet<string>(music_Context.song.Select(x => x.File_Path));
            foreach (var newSong in New_Songs)
            {
                var i = New_Songs.Last();
                if (newSong != null && currentHashSet.Contains(newSong.File_Path) == false)
                {
                    music_Context.song.Add(newSong);
                    currentHashSet.Add(newSong.File_Path);
                }
            }
        }

        public void CreateArtistRelationships()
        {
            var artistsInDatabase = new HashSet<string>(music_Context.artist.Select(x => x.title));
            var artistsFromSongs = new HashSet<string>(music_Context.song.Select(x => x.Artist));
            var newArtists = artistsFromSongs.Except(artistsInDatabase);

            foreach (var title in newArtists)
            {
                var temp = new Artist(title);
                music_Context.artist.Add(temp);
            }
        }

        private async void CompariTitles<T>(IEnumerable<string> DatabasemetadataList, IEnumerable<string> SongMetadataList, Func<Task> addToDatabase) where T : Metadata, new()
        {
            var databaseHashSet = new HashSet<string>(DatabasemetadataList);
            var songHashSet = new HashSet<string>(SongMetadataList);
            var titlesToAdd = songHashSet.Except(databaseHashSet).ToArray();

            for (int i = 0; i < titlesToAdd.Length; i++)
            {
                var temp = new T();
                temp.title = titlesToAdd[i];
                //addToDatabase(temp);
            }
        }

        public void CreateGenreRelationships()
        {
            var genresInDatabase = new HashSet<string>(music_Context.genre.Select(x => x.title));
            var genresFromSongs = new HashSet<string>(music_Context.song.Select(x => x.Genre));
            var newGenres = genresFromSongs.Except(genresInDatabase);

            foreach (var title in newGenres)
            {
                var temp = new Genre(title);
                music_Context.genre.Add(temp);
            }
        }

        public void CreateAlbumRelationships()
        {
            // var Album_Titles = music_Context.song
            // .Select(x => x.Album).Distinct()
            // .Except(music_Context.album
            // .Select(x => x.title));

            // foreach (var title in Album_Titles)
            // {

            //     var albumArtists = music_Context.song
            //     .Where(x => x.Album == title)
            //     .Select(x => x.Artist)
            //     .Distinct();
            //     var b = albumArtists.ToArray();
            //     foreach (var artist in albumArtists)
            //     {
            //         var temp = new Album(title, artist);
            //         music_Context.album.Add(temp);
            //     }

            // }
            var albumsInDatabase = new HashSet<string>(music_Context.album.Select(x => x.title));
            var albumsFromSongs = new HashSet<string>(music_Context.song.Select(x => x.Album));
            var newAlbum = albumsFromSongs.Except(albumsInDatabase);

            foreach (var title in newAlbum)
            {

                var albumArtists = music_Context.song
                .Where(x => x.Album == title)
                .Select(x => x.Artist)
                .Distinct();

                foreach (var artist in albumArtists)
                {
                    var temp = new Album(title, artist);
                    music_Context.album.Add(temp);
                }

            }


        }


        public void Check_Old_Songs(string[] Current_Song_Paths)
        {

            var Current_Path_Hash_Table = new HashSet<string>(Current_Song_Paths);
            var Songs_In_Database = music_Context.song.ToDictionary(k => k.File_Path, v => v);
            var oldSongPaths = Songs_In_Database.Keys
            .Except(Current_Song_Paths);
            var oldSongObject = new List<Song>();
            foreach (var path in oldSongPaths)
            {
                oldSongObject.Add(Songs_In_Database[path]);
            }
            music_Context.song.RemoveRange(oldSongObject);

            //todo: update artist,genres and albums

            var newSongPaths = Current_Song_Paths.Except(Songs_In_Database.Keys);
            foreach (var Song_Path in newSongPaths)
            {
                var song = FileManagment.Parse_Audio_String(Song_Path);
                if (song != null)
                {
                    music_Context.song.Add(song);
                    
                }


            }
            DeleteEmptyArtist();
            DeleteEmptyGenre();
            DeleteEmptyAlbum();
        }

        public void DeleteEmptyArtist()
        {
            var artist = music_Context.artist.ToDictionary(k => k.title, v => v);
            var oldArtistTitles = artist.Keys
            .Except(music_Context.song.Select(x => x.Artist)).ToArray();
            var artistsToDelete = new List<Artist>();
            foreach (var Title in oldArtistTitles)
            {
                artistsToDelete.Add(artist[Title]);
            }
            music_Context.artist.RemoveRange(artistsToDelete);
        }
        public void DeleteEmptyGenre()
        {
            var genre = music_Context.genre.ToDictionary(k => k.title, v => v);
            var oldGenreTitles = genre.Keys
            .Except(music_Context.song.Select(x => x.Genre)).ToArray();
            var genreToDelete = new List<Genre>();
            foreach (var Title in oldGenreTitles)
            {
                genreToDelete.Add(genre[Title]);
            }
            music_Context.genre.RemoveRange(genreToDelete);
        }

        public void DeleteEmptyAlbum()
        {
            var album = music_Context.album.ToDictionary(k => k.title, v => v);
            var oldAlbumTitles = album.Keys
            .Except(music_Context.song.Select(x => x.Album)).ToArray();
            var albumToDelete = new List<Album>();
            foreach (var Title in oldAlbumTitles)
            {
                albumToDelete.Add(album[Title]);
            }
            music_Context.album.RemoveRange(albumToDelete);
        }


        public string CheckIfTitleIsDefined(string Metadata_Title)
        {
            string newTitle = "Unknown";
            if (string.IsNullOrEmpty(Metadata_Title) == false)
                newTitle = Metadata_Title;
            return newTitle;

            var artist = music_Context.artist.ToList();
            artist.ForEach(x =>
            {
                if (x.Get_Related_Songs(music_Context).Count() == 0)
                {
                    music_Context.artist.Remove(x);
                }
            });
        }

        /// <summary>
        /// Gets and sets the name of the Music Database / playlist as a string
        /// </summary>
        /// <returns>current name of the database </returns>
        public string Database_Name
        {
            get
            {
                return database_Name;
            }
        }

        public IMusic_Context MusicDatabase_Context1 { get => music_Context; set => music_Context = value; }


        /// <summary>
        /// check if there is nothing in the tag if so set the tag to blank
        /// </summary>
        /// <param name="tag_To_Check">the tag to check</param>
        /// <returns>string Blank if the tag is null </returns>
        public static object Check_Null_Metadata_TAG(object tag_To_Check)
        {
            if (tag_To_Check == null)
                return "Blank";
            else
                return tag_To_Check;
        }
        /// <summary>
        /// get rid of capital letters,spaces and hyphens in the song tag
        /// </summary>
        /// <param name="Tag_To_Normalize">string value of the tag</param>
        /// <returns>the new tag normalized </returns>
        public static string Normalize_Tags(string Tag_To_Normalize)
        {
            //TODO: Only check against the normalized name (MusicBrains Bug )
            string[] Character_Filter = new string[] { " ", "-" };
            Tag_To_Normalize = Tag_To_Normalize.ToLower();

            foreach (string sr in Character_Filter)
            {
                Tag_To_Normalize = Regex.Replace(Tag_To_Normalize, sr, "");
            }
            return Tag_To_Normalize;
        }





        // /// <summary>
        // /// loads the data from the sqlfile and table named song to the current objects datatable
        // /// </summary>
        // public DataTable populateDatatable(string TableName, DataTable Table_To_Populate)
        // {
        //     using (var Current_Connection = new SQLiteConnection(Music_DataTables.Database_Connection_String))
        //     using (var current_Adapter = new SQLiteDataAdapter("SELECT * FROM " + TableName, Current_Connection))
        //     using (new SQLiteCommandBuilder(current_Adapter))
        //     {
        //         current_Adapter.Fill(Table_To_Populate);
        //         return Table_To_Populate;
        //     }
        // }

        // public void save_Datatable(string TableName, DataTable Table_To_Save)
        // {
        //     using (var Current_Connection = new SQLiteConnection(Music_DataTables.Database_Connection_String))
        //     using (var current_Adapter = new SQLiteDataAdapter("SELECT * FROM " + TableName, Current_Connection))
        //     using (new SQLiteCommandBuilder(current_Adapter))
        //     {
        //         Current_Connection.Open();
        //         current_Adapter.Update(Table_To_Save);
        //     }
        // }

        public Artist FindArtist(string Name)
        {
            return music_Context.artist.FirstOrDefault(x => x.title == Name);
        }
        public Genre FindGenre(string Name)
        {
            return music_Context.genre.FirstOrDefault(x => x.title == Name);
        }



        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }

    public class Playlist : Song_Database
    {
        private double _Playlist_Length;
        private string _File_Path;
        private List<Genre> genres = new List<Genre>();
        private List<Artist> artists = new List<Artist>();
        private List<Song> songs = new List<Song>();

        private Random Random_Index;

        private Song_Database database;

        public double Playlist_Length
        {
            get
            {
                return _Playlist_Length;
            }

            set
            {
                _Playlist_Length = value;
            }
        }

        public string File_Path
        {
            get
            {
                return _File_Path;
            }

            set
            {
                _File_Path = value;
            }
        }

        public List<Song> Songs { get => songs; set => songs = value; }

        /// <summary>
        ///creates a playlist based on the given genres
        /// </summary>
        /// <param name="Playlist_Name">name of playlist to be created</param>
        /// <param name="Genre_Index"> similar genres </param>
        /// <param name="Playlist_Length">length of playlist in minutes </param>
        public Playlist(string Playlist_Name, Metadata First, Genre[] Genre_Index, double Playlist_Length, Song_Database database)
             : base(Playlist_Name)
        {
            Random_Index = Start_Playlist_Randomness();

            genres.AddRange(Genre_Index);
            List<Song> Genre_Songs = new List<Song>();
            this.database = database;
            foreach (Genre i in Genre_Index)
            {
                if (i.Get_Related_Songs(database.MusicDatabase_Context1) != null)
                    Genre_Songs.AddRange(i.Get_Related_Songs(database.MusicDatabase_Context1));
            }
            Generate_Playlist(First, Genre_Songs, database.MusicDatabase_Context1);
        }

        /// <summary>
        /// creates a type playlist
        /// </summary>
        /// <param name="Playlist_Name">name of the new playlist</param>
        /// <param name="Assosiated_Artists">artist the playlist is going to be made out of</param>
        /// <pafram name="Playlist_Length">how long the playlist should last</param>
        public Playlist(string Playlist_Name, Metadata First, Artist[] Assosiated_Artists, double Playlist_Length, Song_Database database)
             : base(Playlist_Name)
        {
            Random_Index = Start_Playlist_Randomness();
            artists.AddRange(Assosiated_Artists);
            List<Song> Artist_Songs = new List<Song>();
            this.database = database;
            foreach (Artist i in Assosiated_Artists)
            {
                Artist_Songs.AddRange(i.Get_Related_Songs(database.MusicDatabase_Context1));
            }
            Generate_Playlist(First, Artist_Songs, database.MusicDatabase_Context1);
        }



        /// <summary>
        /// constructor for reading in playlist files
        /// </summary>
        /// <param name="Playlist_Name">name of the new playlist</param>
        /// <param name="Music_To_Add">list of music files to add to playlist </param>
        public Playlist(string Playlist_Name, Song[] Music_To_Add, string Playlist_File, Song_Database database)
            : base(Playlist_Name)
        {
            // distinct because a playlist may accidentally have the same song in (if made outside of the program )

            songs.AddRange(Music_To_Add.Distinct());
            this.Playlist_Length = Music_To_Add.Sum(x => x.Song_Length);

            // get all the genres into a list
            var Genres = Music_To_Add.Select(x => database.FindGenre(x.Genre)).Distinct();
            genres.AddRange(Genres);
            // gets all artists into a list and joins the 2 removing duplicates
            var Artists = Music_To_Add.Select(x => database.FindArtist(x.Artist)).Distinct();
            artists.AddRange(Artists);

            this.File_Path = Playlist_File;
        }

        /// <summary>
        /// creates a random sequence using the current times
        /// </summary>
        /// <returns>random sequence based of the seconds component of the time now</returns>
        private static Random Start_Playlist_Randomness()
        {
            int Random_Seed = DateTime.Now.Second;
            Random rnd = new Random(Random_Seed);
            return rnd;
        }

        /// <summary>
        /// selects a list of random songs given a list of songs to pick from
        /// </summary>
        /// <param name="Avalible_Songs">list of Songs to be selected from</param>
        /// <param name="Playlist_Length"> how long the playlist  should be </param>
        /// <param name="Real_Playlist_Length">how long the playlist actually is (since if there isn't enough songs this method will return available songs ) </param>
        /// <returns></returns>
        private void Generate_Playlist(Metadata First_Song, List<Song> Avalible_Songs, IMusic_Context context)
        {
            Random Random_Index = Start_Playlist_Randomness();

            Song First_Track = First_Song.Get_Related_Songs(context)[Random_Index.Next(0, First_Song.Get_Related_Songs(context).Count)];
            songs.Add(First_Track);
            double Currnt_Playlist_Length = First_Track.Song_Length;

            if (Avalible_Songs.Contains(First_Track))
                Avalible_Songs.Remove(First_Track);

            while (Currnt_Playlist_Length <= Playlist_Length && Avalible_Songs.Count() > 0)
            {
                int Random_Index_Select = Random_Index.Next(0, Avalible_Songs.Count() - 1);
                Song New_Song = Avalible_Songs[Random_Index_Select];
                songs.Add(New_Song);
                Avalible_Songs.Remove(New_Song);
                Currnt_Playlist_Length += New_Song.Song_Length;
            }

            Playlist_Length = Currnt_Playlist_Length;
        }

        /// <summary>
        /// writes out an m3u8 playlist
        /// </summary>
        /// <param name="File_Name">path to save playlist </param>
        public void Write_Out_Playlist(string File_Path)
        {
            using (System.IO.TextWriter Playlist_File = new System.IO.StreamWriter(File_Path))
            {
                try
                {
                    Playlist_File.WriteLine("# This Playlist was made by Playlister on " + DateTime.Now.Date.ToString());
                    foreach (Song s in songs)
                    {
                        Playlist_File.WriteLine(s.File_Path);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                    //throw new Failed_To_Write_File_Exeption("There was a fault saving file");
                }
                finally
                {
                    Playlist_File.Dispose();
                }
            }
        }

        /// <summary>
        /// creates a playlist based of the XSPF playlist file
        /// </summary>
        /// <param name="Playlist_File_Path">path of the playlist file </param>
        /// <param name="Database_To_Add_To">database related to the songs </param>
        /// <returns>new playlist using the new playlist file </returns>
        public static Playlist Read_XSPF_Playlist_File(string Playlist_File_Path, Song_Database Database_To_Add_To)
        {
            try
            {
                using (var Playlist_File_Stream = new StreamReader(Playlist_File_Path))
                {
                    XDocument Playlist_Document = XDocument.Load(Playlist_File_Stream);
                    List<Song> Songs_To_Add = new List<Song>();

                    var Playlist_Songs = from Attribute in Playlist_Document.Descendants("track")
                                         select new
                                         {
                                             Path = Attribute.Element("location").Value
                                         };

                    //for-each element in playlist songs add path to path strings
                    string[] Path_Strings = Playlist_Songs.Select(x => x.Path).ToArray();

                    Song[] Song_index = Parse_Playlist_Strings(Path_Strings, Database_To_Add_To);

                    string Playlist_Name = Path.GetFileName(Playlist_File_Path);
                    Playlist New_Playlist = new Playlist(Playlist_Name, Song_index, Playlist_File_Path, Database_To_Add_To);
                    return New_Playlist;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                string Playlist_Name = Path.GetFileName(Playlist_File_Path);
                throw new Failed_To_Read_File_Exeption("Failed to read file" + Playlist_Name);
            }
        }

        /// <summary>
        /// creates a playlist based of the M3u and m3u8 playlist file
        /// </summary>
        /// <param name="Playlist_File_Path">path of the playlist file </param>
        /// <param name="Database_To_Add_To">database related to the songs </param>
        /// <returns>new playlist using the new playlist file </returns>
        public static Playlist Read_M38U_Playlist_File(string Playlist_File_Path, Song_Database Database_To_Add_To)
        {
            Playlist New_Playlist = null;
            try
            {
                var Raw = File.ReadAllLines(Playlist_File_Path, System.Text.Encoding.UTF8);
                List<string> Path_Strings = Raw.ToList();
                List<string> Corrected_Strings = new List<string>();
                foreach (string Line in Path_Strings)
                {
                    if (Line.First() != '#')
                    {
                        if (Relative_Path(Line) == false)
                            Corrected_Strings.Add(Line);
                        else
                        {
                            string Drive = System.IO.Path.GetDirectoryName(Playlist_File_Path);
                            string Corrected_Line = Drive + "\\" + Line;
                            Corrected_Strings.Add(Corrected_Line);
                        }
                    }
                }
                Song[] Song_index = Parse_Playlist_Strings(Corrected_Strings.ToArray(), Database_To_Add_To);
                string Playlist_Name = Path.GetFileName(Playlist_File_Path);
                New_Playlist = new Playlist(Playlist_Name, Song_index, Playlist_File_Path, Database_To_Add_To);
            }
            catch (Exception e)
            {
                Console.WriteLine("falild to read file {0}  error {1}", Playlist_File_Path, e.Message);

                //  throw new Failed_To_Read_File_Exeption("Failed to read file" + Playlist_File + "\n" + e.InnerException);
            }

            return New_Playlist;
        }

        /// <summary>
        /// converts strings of paths into a array of songs
        /// </summary>
        /// <param name="Data"> array of strings to be parsed </param>
        /// <param name="Database_To_Add_To"> database related to songs</param>
        /// <returns></returns>
        public static Song[] Parse_Playlist_Strings(string[] Data, Song_Database Database_To_Add_To)
        {
            List<Song> Songs_To_Add = new List<Song>();
            for (int i = 0; i < Data.Length; i++)
            {
                string Song_Path = Data[i];
                string Forward_Slash = @"/";
                if (Song_Path.Contains(Forward_Slash))
                {
                    Song_Path = System.Text.RegularExpressions.Regex.Replace(Song_Path, Forward_Slash, @"\");
                }
                //TODO: HTTP Filter
                var Current_Music_Database = Database_To_Add_To.MusicDatabase_Context1.song.ToList();

                if (Current_Music_Database.Any(x => x.Get_Path() == Song_Path))
                {
                    Song Song_To_Add_Playlist = Current_Music_Database.FirstOrDefault(x => x.Get_Path() == Song_Path);
                    Songs_To_Add.Add(Song_To_Add_Playlist);
                }
                else
                {
                    try
                    {
                        using (var Audio_Stream = File.Open(Song_Path, FileMode.Open))
                        {
                            TagLib.File Track_Temp = TagLib.File.Create(Song_Path);

                            string New_Title = Track_Temp.Tag.Title;
                            double New_Length_Minutes = Math.Round(Track_Temp.Properties.Duration.TotalMinutes, 2);
                            string New_Genre_Name;
                            string New_Artist_Name;
                            string New_Album_Name;

                            New_Title = Playlister_Class_Library.Song_Database.Check_Null_Metadata_TAG(New_Title).ToString();
                            New_Genre_Name = Playlister_Class_Library.Song_Database.Check_Null_Metadata_TAG(Track_Temp.Tag.FirstGenre).ToString();
                            New_Artist_Name = Playlister_Class_Library.Song_Database.Check_Null_Metadata_TAG(Track_Temp.Tag.FirstAlbumArtist).ToString();
                            New_Album_Name = Playlister_Class_Library.Song_Database.Check_Null_Metadata_TAG(Track_Temp.Tag.Album).ToString();
                            string New_Path = Song_Path;

                            Song New_Track = new Song(New_Title, New_Artist_Name, New_Length_Minutes, New_Genre_Name, New_Album_Name, New_Path);
                            Songs_To_Add.Add(New_Track);
                            //Debug.WriteLine(New_Track.Get_Name());
                        }
                    }
                    catch (Exception e)
                    {
                        if (e is TagLib.UnsupportedFormatException || e is TagLib.CorruptFileException)
                        {
                            Debug.WriteLine("Song_skiped " + Song_Path);
                            continue;
                        }

                        throw e;
                    }
                }
            }
            return Songs_To_Add.ToArray();
        }

        public static bool Relative_Path(string path)
        {
            if (System.IO.Path.IsPathRooted(path) && !System.IO.Path.GetPathRoot(path).Equals(System.IO.Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal))
                return false;
            else
                return true;
        }
    }
}

public class Failed_To_Write_File_Exeption : Exception
{
    public Failed_To_Write_File_Exeption()
    { }

    public Failed_To_Write_File_Exeption(string message)
        : base(message)
    { }

    public Failed_To_Write_File_Exeption(string message, Exception inner)
        : base(message, inner)
    { }
}

public class Failed_To_Read_File_Exeption : Exception
{
    public Failed_To_Read_File_Exeption()
    { }

    public Failed_To_Read_File_Exeption(string message)
        : base(message)
    { }

    public Failed_To_Read_File_Exeption(string message, Exception inner)
        : base(message, inner)
    { }
}

// /// <summary>
// /// sets the song index from the song database
// /// </summary>
// /// <param name="New_Music_Index">the new dictionary the music index must be set to</param>
// /// <returns>dictionary of songs where the key is the song's file path</returns>
// public async Task Set_Music_Index_With_Metadata(Dictionary<string, Song> New_Music_Index)
// {
//     foreach (var data in New_Music_Index)
//     {
//         Song New_Song = data.Value;
//         New_Song.Set_Artist(New_Song.Get_Artist().Get_Title(), this);
//         New_Song.Set_Genre(New_Song.Get_Genre().Get_Title(), this);
//         New_Song.Get_Artist().Add_Song_Extract_Metadata(New_Song);
//         await New_Song.Get_Artist().Add_Song_To_Album(New_Song, New_Song.Get_Album().Get_Title());
//         this.Add_Song(New_Song);
//     }
// }


// /// <summary>
// /// loads metadata values from XML
// /// </summary>
// public void Load_Metadata_Relations()
// {
//     XDocument ID_Document = XDocument.Load(Metadata.Artist_Path);

//     var ID_Data = from Attribute in ID_Document.Descendants("Artist")
//                   select new
//                   {
//                       Title = Attribute.Element("Title").Value,
//                       MusicBrainz_ID = Attribute.Element("MusicBrainz_ID").Value,
//                       List_Of_Aliese = Attribute.Element("ArtistAliase"),
//                   };

//     foreach (var Artist in ID_Data)
//     {
//         string Artist_Title = XmlConvert.DecodeName(Artist.Title);
//         string Artist_MusicBrainz_Id = Artist.MusicBrainz_ID;
//         this.Get_Artists_Index().FirstOrDefault(x => x.Get_Title() == Artist_Title).Set_MusicBrainz_ID(Artist.MusicBrainz_ID);
//     }
// }

// /// <summary>
// /// writes out all songs in the song index to an XML file
// /// </summary>
// /// <param name="File_Name">location of the database file</param>
// public async void Write_Music_Database_XML()
// {
//     StorageFile Music_Database = await Get_Music_Database_File();
//     if (Music_Database == null)
//     {
//         var Local_Folder = Windows.Storage.ApplicationData.Current.LocalFolder;
//         Music_Database = await Local_Folder.CreateFileAsync(Music_DataBase_Name);
//     }

//     var Stream = await Music_Database.OpenStreamForWriteAsync();
//     using (XmlWriter Save_Music = XmlWriter.Create(Stream, new XmlWriterSettings() { Async = true }))
//     {
//         await Save_Music.WriteStartDocumentAsync();
//         Save_Music.WriteStartElement("Song_Metadata");

//         foreach (KeyValuePair<string, Song> Song in Music_Index)
//         {
//             await Song.Value.Write_Xml(Save_Music);
//         }
//         await Save_Music.WriteEndElementAsync();
//         await Save_Music.WriteEndDocumentAsync();
//     }
// }

// /// <summary>
// /// Saves Genres related arrays to an XML file
// /// </summary>
// /// <param name="File_Name">destination of the XML file to be saved </param>
// /// <returns>whether the file successfully saved </returns>
// public async Task<bool> Write_Genre_Indexes_Xml()
// {
//     StorageFile Genre_Metadata_File = await StorageFile.GetFileFromApplicationUriAsync(new Uri(Metadata.Genre_Path));
//     var Stream = await Genre_Metadata_File.OpenStreamForWriteAsync();
//     XmlWriter Save_Metadata = XmlWriter.Create(Stream);
//     try
//     {
//         Save_Metadata.WriteStartElement("Metadata_Index");
//         Save_Metadata.WriteStartElement("Genre_Index");
//         foreach (Genre G in Genre_Index)
//         {
//             G.save(Save_Metadata);
//         }
//         Save_Metadata.WriteEndElement();

//         return true;
//     }
//     catch (Exception e)
//     {
//         throw e;
//     }
//     finally
//     {
//         Save_Metadata.Dispose();
//     }
// }

// /// <summary>
// /// Saves Artists related arrays to an XML file
// /// </summary>
// /// <param name="File_Name">destination of the XML file to be saved </param>
// /// <returns>whether the file successfully saved </returns>
// public async Task<bool> Write_Aritist_Indexes_Xml()
// {
//     StorageFile Artist_Metadata_File = await StorageFile.GetFileFromApplicationUriAsync(new Uri(Metadata.Artist_Path));
//     var Stream = await Artist_Metadata_File.OpenStreamForWriteAsync();
//     XmlWriter Save_Metadata = XmlWriter.Create(Stream);
//     try
//     {
//         Save_Metadata.WriteStartElement("Artist_Index");
//         foreach (Artist A in Artist_Index)
//         {
//             A.save(Save_Metadata);
//         }
//         Save_Metadata.WriteEndElement();

//         return true;
//     }
//     catch (Exception e)
//     {
//         throw e;
//     }
//     finally
//     {
//         Save_Metadata.Dispose();
//     }
// }

/// <summary>
/// saves Music Index, Artist Index and Genre index
/// </summary>
/// <returns>async task to complete </returns>
/// 

#region Metadata tag filtering

/*
// tries to make similar tags e.g hip-hop and hiphop to be the same tag
public static Song_DataBase Normalize_Tags(Song_DataBase Database_To_Normalize)
{
	string[] Character_Filter = new string[] { " ", "-", @"\t" };

	IEnumerable<Metadata> Clense_Names( IEnumerable<Metadata> NameS_To_Clense)
	{
		foreach (Metadata Current_MetaData in NameS_To_Clense)
		{
			string New_Name = Current_MetaData.Get_Title().ToLower();
			foreach (string Filter in Character_Filter)
			{
				New_Name = Regex.Replace(New_Name, Filter, "");
			}
			Current_MetaData.Set_Title(New_Name);
		}
		return NameS_To_Clense;
	}

	IEnumerable<Metadata> Merge_Names( IEnumerable<Metadata> NameS_To_merge)
	{
	foreach(Metadata item in NameS_To_merge)
	{
		var Duplicate_Metadata = NameS_To_merge.Where( x => item.Get_Title() == x.Get_Title());
		if (Duplicate_Metadata.Count() > 1 )
		{
			Duplicate_Metadata.First().Merge_Metadata_tag(Duplicate_Metadata.Skip(1).ToList());
		}
	}
	 return  NameS_To_merge.Distinct();
	}

	 // Merge same name metadata

	var Genre_Index = Database_To_Normalize.Get_Genre_Index();
	Genre_Index = (DbSet<Genre>)Clense_Names(Genre_Index);

	// normalize artist
	var Artist_Index = Database_To_Normalize.Get_Artists_Index();
	Artist_Index = (DbSet<Artist>) Clense_Names(Artist_Index);

	//List<Artist> Current_Metadata_Index = new List<Artist>(Artist_Index);
	Genre_Index = (DbSet<Genre>)Merge_Names(Genre_Index);
	Artist_Index = (DbSet<Artist>)Merge_Names(Artist_Index);

	// Merge same name artists
	Database_To_Normalize.MusicDatabase_Context.genre = Genre_Index;
	Database_To_Normalize.MusicDatabase_Context.artist = Artist_Index;

	return Database_To_Normalize;
}
*/



#endregion Metadata tag filtering

// public async void Serialize_Albums()
// {
//     StorageFile Album_Metadata_File = await StorageFile.GetFileFromApplicationUriAsync(new Uri("Album_Metadata.xml"));
//     using (XmlWriter Writer = XmlWriter.Create(await Album_Metadata_File.OpenStreamForWriteAsync()))
//     {
//         System.Xml.Serialization.XmlSerializer Serializer = new System.Xml.Serialization.XmlSerializer(typeof(Album));
//         var Album_Collection = this.Artist_Index.Select(x => x.Get_Artist_Albums().Select(y => y));
//         Serializer.Serialize(Writer, Album_Collection);
//         Writer.Dispose();
//     }
// }