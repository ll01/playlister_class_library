using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System;

namespace Playlister_Class_Library
{
    public class Artist : Metadata, IContext_Object
    {
        private string MusicBrainz_STUFF = "Blank";
        private List<string> Artist_Alias = new List<string>();

        [Column]
        public override string title { get; set; }

        public int id { get; private set; }

        public string Get_MusicBrainz_ID()
        {
            //TODO: FIX TITLEING IN SQLITE
            return MusicBrainz_STUFF;
        }

        public string Set_MusicBrainz_ID(string New_ID)
        {
            //  if (MusicBrainz_Id != "Blank" || MusicBrainz_Id != "ID can't be found")
            MusicBrainz_STUFF = New_ID;

            return MusicBrainz_STUFF;
        }

        public Artist(string Artist_Name) : base(Artist_Name)
        {
            title = Artist_Name;
        }

        public Artist() : base("nil")
        {
        }

        public Artist(int id, string Artist_Name) : base(Artist_Name)
        {
            title = Artist_Name;
            this.id = id;
        }



        /// <summary>
        /// get list of albums related to this artist
        /// </summary>
        /// <returns>list of object album related to artist</returns>
        public List<Album> Get_Artist_Albums(IMusic_Context context)
        {
            return context.album.Where(x => x.Album_Artist == title).ToList();
        }

        public async Task Save_Metadata_Ids(XmlWriter Save_Id)
        {
            await Save_Id.WriteStartElementAsync(null, "MusicBrainz_id", null);
            await Save_Id.WriteElementStringAsync(null, this.title, null, MusicBrainz_STUFF);
            await Save_Id.WriteEndElementAsync();
        }

        public string Get_Title()
        {
            return title;
        }

        public int Get_ID()
        {
            return id;
        }

        public override List<Song> Get_Related_Songs(IMusic_Context context)
        {
            return context.song.Where(x => x.Artist == title).ToList();
        }

        public override List<Genre> Get_Related_Genres(IMusic_Context context)
        {
            var relatedGenreTitles = Get_Related_Songs(context)
            .Select(x => x.Genre)
            .Distinct();
            var genreDictionary = context.genre.ToDictionary(k => k.title, v => v);
            var relatedGenres  = new List<Genre>();
            foreach (var title in relatedGenreTitles)
            {
                Genre temp ;
                if(genreDictionary.TryGetValue(title, out temp)) {
                    relatedGenres.Add(temp);
                }
            }
            
            return relatedGenres;
        }

        public override List<Artist> Get_Related_Artists(IMusic_Context context)
        {
           return  Get_Related_Genres(context)
           .SelectMany(x  => x.Get_Related_Artists(context))
           .ToList();
        }
    }
}