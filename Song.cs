﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Playlister_Class_Library
{
	public class Song : IEquatable<Song>
	{
		public int id { get; private set; }

		public string title { get; private set; }

		private string artist;
		private double song_Length;
		private string genre;
		private string album;

		[Column("path")]
		public string File_Path { get; private set; }
		[Column]
		public string Artist { get => artist; set => artist = value; }
		[Column]
		public double Song_Length { get => song_Length; set => song_Length = value; }
		[Column]
		public string Genre { get => genre; set => genre = value; }
		[Column]
		public string Album { get => album; set => album = value; }

		//private bool New_Song;

		//call the Create_Song method to create a song not the constructor directly
		/// <summary>
		/// Creates a song object DO NOT CALL: this method dose not add it self to associated genre and artist.
		/// </summary>
		/// <param name="New_Name"> name of song </param>
		/// <param name="New_Artist"> related artist </param>
		/// <param name="New_Song_Length"> length of song in minutes </param>
		/// <param name="New_Genre"> associated genre of song </param>
		/// <param name="New_Path">URL of song object </param>
		public Song(string New_Name, string New_Artist, double New_Song_Length, string New_Genre, string New_Album, string New_Path)
		{
			this.title = New_Name;
			this.Song_Length = New_Song_Length;
			this.File_Path = New_Path;
			genre = New_Genre;
			artist = New_Artist;
			album = New_Album;


		}

		public Song()
		{
		}

		/// <summary>
		/// gets the absolute path where the song is stored
		/// </summary>
		/// <returns>string where song exists</returns>
		public string Get_Path()
		{
			return File_Path;
		}

		public override string ToString()
		{
			return this.title;
		}

		public byte[] Retrive_Thumbnail()
		{
			System.IO.MemoryStream Thumbnail_Memorystream = new System.IO.MemoryStream();

			try
			{

				var Audio = TagLib.File.Create(File_Path);
				if (Audio.Tag.Pictures.Count() != 0)
				{
					Thumbnail_Memorystream = new MemoryStream(Audio.Tag.Pictures[0].Data.Data);
				}

				return Thumbnail_Memorystream.ToArray();
			}
			catch
			{
				System.Diagnostics.Debug.WriteLine("Get Thumbnail failed" + this.title);
				return null;
			}
		}

		public override bool Equals(object obj)
		{
			Song Object_To_Equals = obj as Song;
			// since  the artist and genre tags are equaled by values in there related song / genre lists just compare the name / to string
			if ((Object_To_Equals.title == title) && (Object_To_Equals.Artist.ToString() == artist) && (Object_To_Equals.Song_Length == Song_Length) && (Object_To_Equals.Genre.ToString() == genre) && (Object_To_Equals.File_Path == File_Path))
				return true;
			else
				return false;
		}

		public bool Equals(Song obj)
		{
			Song Object_To_Equals = obj;
			// since  the artist and genre tags are equaled by values in there related song / genre lists just compare the name / to string
			if ((Object_To_Equals.title == title) && (Object_To_Equals.Artist.ToString() == Artist) && (Object_To_Equals.Song_Length == Song_Length) && (Object_To_Equals.Genre.ToString() == Genre) && (Object_To_Equals.File_Path == File_Path))
				return true;
			else
				return false;
		}

		public override int GetHashCode()
		{
			int Hash = title.GetHashCode() * 17 + Song_Length.GetHashCode() + File_Path.GetHashCode();
			return Hash;
		}

	}
}