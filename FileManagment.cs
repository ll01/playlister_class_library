﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
  using System.Collections.Concurrent;


namespace Playlister_Class_Library
{
    public class FileManagment
    {
        static List<string> failedSongPaths = new List<string>();
        public static List<string> Get_All_Music_Files(string path)
        {
            string[] Audio_File_Extentions = new string[] { ".mp3", ".m4a", ".flac", ".aac" };

            //read in data
            List<string> Audio_File_Paths = new List<string>();
            foreach (string Extension in Audio_File_Extentions)
            {
                var Audio_Extension_Files = Directory.GetFiles(path, "*" + Extension + "*", SearchOption.AllDirectories);
                // if the get file method returns nothing ignore it
                if (Audio_Extension_Files.Length == 0 || Audio_Extension_Files == null)
                    continue;
                else
                    //Audio_File_Paths.AddRange(Audio_Extension_Files);
                    Audio_File_Paths = Audio_File_Paths.Concat(Audio_Extension_Files).ToList();
            }

            //TODO: Optemize work
            return Audio_File_Paths;
        }

        public static List<string> Get_All_Playlist_Files(string[] Current_Paths)
        {
            string[] Playlist_File_Extentions = new string[] { "*.M3U", "*.M3U8" };
            //read in data
            List<string> Audio_File_Paths = new List<string>();
            foreach (string path in Current_Paths)
                foreach (string Extension in Playlist_File_Extentions)
                {
                    var Audio_Extension_Files = Directory.GetFiles(path, Extension, SearchOption.AllDirectories);
                    // if the get file method returns nothing ignore it
                    if (Audio_Extension_Files.Length == 0 || Audio_Extension_Files == null)
                        continue;
                    else
                        //Audio_File_Paths.AddRange(Audio_Extension_Files);
                        Audio_File_Paths = Audio_File_Paths.Concat(Audio_Extension_Files).Distinct().ToList();
                }

            return Audio_File_Paths;
        }





        public static Song_Database Read_Audio_Files_via_Taglibsharp(string[] Music_Directorys, IMusic_Context context)
        {
            var All_song_paths = new List<string>();
            for (int i = 0; i < Music_Directorys.Length; i++)
            {
                var Temp_Paths = Get_All_Music_Files(Music_Directorys[i]);
                All_song_paths.AddRange(Temp_Paths);
            }
            var All_song_paths_Array = All_song_paths.ToArray();
            var Song_Database = new Song_Database(DateTime.Now.ToString() + " databasse", context);
            var isFresh = context.song.Count() == 0;
            if (isFresh == true)
            {
                var Songs_To_Add = new ConcurrentBag<Song>();
                var Genres_To_Add = new List<Genre>();
                var itemCreattionBench = new Stopwatch();
                itemCreattionBench.Start();
                Parallel.For(0, All_song_paths_Array.Length, i =>
               {
                   var New_Song = Parse_Audio_String(All_song_paths[i]);
                   if (New_Song != null)
                   {
                           Songs_To_Add.Add(New_Song); 
                       
                       //var metadata = ParseMetadata(New_Song.Genre, New_Song.Artist, New_Song.Album);
                       //Genres_To_Add.AddRange(metadata.genres);

                   }
               });
                Song_Database.Add_Songs(Songs_To_Add.ToArray());
             
                Song_Database.MusicDatabase_Context1.genre.AddRange(a);
                itemCreattionBench.Stop();
                // Console.WriteLine(" item creation time {0} ", itemCreattionBench.Elapsed);
                // CreateMetadata<Artist>(Songs_To_Add.ToArray(), x =>
                // {
                //     var artist = x as Artist;
                //     Song_Database.MusicDatabase_Context1.artist.Add(artist);
                // });
                // CreateMetadata<Genre>(Songs_To_Add.ToArray(), x =>
                // {
                //     var genre = x as Genre;
                //     Song_Database.MusicDatabase_Context1.genre.Add(genre);
                // });
                // var AlbumHashSet = new HashSet<Album>();
                // CreateMetadata<Album>(Songs_To_Add.ToArray(), x =>
                // {
                //     var album = x as Album;
                //     Song_Database.MusicDatabase_Context1.album.Add(album);
                // });
            }
            else
            {
                Song_Database.Check_Old_Songs(All_song_paths_Array);
            }
            return Song_Database;

        }



        /// <summary>
        /// Rips Metadata in order of  ew_Title, New_Artist_Name , New_Length_Minutes.ToString(), New_Genre_Name, New_Album_Name, Audio_File_Path
        /// </summary>
        /// <param name="Audio_File_Path">Path of the audio file </param>
        /// <returns>all of the metadata to create a song object as a string array </returns>
        public static Song Parse_Audio_String(string Audio_File_Path)
        {
            Song song = null;

            try
            {
                // create a instance of taglib and then rip of all metadata needed.
                TagLib.File Track_Temp = TagLib.File.Create(Audio_File_Path);
                string New_Title = Track_Temp.Tag.Title;
                double New_Length_Minutes = Math.Round(Track_Temp.Properties.Duration.TotalMinutes, 2);
                string New_Genre_Name;
                string New_Artist_Name;
                string New_Album_Name;

                New_Genre_Name = Song_Database.Check_Null_Metadata_TAG(Track_Temp.Tag.FirstGenre).ToString();
                New_Artist_Name = Song_Database.Check_Null_Metadata_TAG(Track_Temp.Tag.FirstAlbumArtist).ToString();
                New_Album_Name = Song_Database.Check_Null_Metadata_TAG(Track_Temp.Tag.Album).ToString();

                song = new Song(New_Title, New_Artist_Name, New_Length_Minutes, New_Genre_Name, New_Album_Name, Audio_File_Path);


            }
            catch (TagLib.CorruptFileException)
            {
                failedSongPaths.Add(Audio_File_Path);
            }


            return song;
        }

        public static (Genre[] genres, Artist artist, Album album) ParseMetadata
        (string Genre, string Artist, string Album)
        {


            string[] GenreArray = new string[] { Genre };
            if (Genre.Contains(","))
            {
                GenreArray = Genre.Split(',');
            }

            var newGenres = new List<Genre>();
            for (int i = 0; i < GenreArray.Length; i++)
            {
                var temp = new Genre(GenreArray[i]);
                newGenres.Add(temp);
            }
            var newArtist = new Artist(Artist);
            var newAlbum = new Album(Album, Artist);

            return (newGenres.ToArray(), newArtist, newAlbum);
        }



        // public static void CreateGenreMetadata(Song[] songs, IMusic_Context context)
        // {
        //     var bench = new Stopwatch();
        //     bench.Start();

        //     var Genres = songs.Select(x => x.Genre).Distinct().ToArray();

        //     for (int i = 0; i < Genres.Length; i++)
        //     {
        //         var temp = new Genre(Genres[i]);
        //         context.genre.Add(temp);
        //     }
        //     bench.Stop();
        //     Console.WriteLine("artist metadata create time  : {0} ", bench.Elapsed);

        // }

        // public static void CreateMetadata<T>(Song[] songs, Action<T> AddToDatabase) where T : Metadata, new()
        // {
        //     var bench = new Stopwatch();
        //     bench.Start();

        //     if (typeof(T).Name != "Album")
        //     {
        //         var Genres = songs.Select(x => x.GetType().GetProperty(typeof(T).Name).GetValue(x, null)).Distinct().ToArray();

        //         for (int i = 0; i < Genres.Length; i++)
        //         {

        //             var temp = new T();
        //             temp.title = (string)Genres[i];

        //             AddToDatabase(temp);

        //         }
        //     } else {
        //         for (int i = 0; i < songs.Length; i++)
        //         {
        //             var currentSong = songs[i];
        //             var temp= new T();
        //             var tempAlbum = temp as Album;
        //             tempAlbum.title = currentSong.title;
        //             tempAlbum.Album_Artist = currentSong.Artist;
        //             AddToDatabase(tempAlbum as T); 
        //         }
        //     }

        //     bench.Stop();
        //     Console.WriteLine("artist metadata create time  : {0} ms", bench.ElapsedMilliseconds);

        // }



    }
}
