using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Playlister_Class_Library
{
    public class Genre : Metadata, IContext_Object
    {
        [Column]
        public override string title { get; set; }

        public int id { get; private set; }

        /// <summary>
        /// creates a genre object
        /// </summary>
        /// <param name="Genre_Name">name of the genre to be created </param>
        public Genre(string Genre_Name) : base(Genre_Name)
        {
            title = Genre_Name;
        }

        public Genre() : base("nil")
        {
        }

        public int Get_ID()
        {
            return id;
        }

        public string Get_Title()
        {
            return title;
        }

        public override List<Song> Get_Related_Songs(IMusic_Context context)
        {
            return context.song.Where(x => x.Genre == title).ToList();
        }

        public override List<Genre> Get_Related_Genres(IMusic_Context context)
        {
             return  Get_Related_Artists(context)
           .SelectMany(x  => x.Get_Related_Genres(context))
           .ToList();
        }

        public override List<Artist> Get_Related_Artists(IMusic_Context context)
        {
             var relatedArtistTitles = Get_Related_Songs(context)
          .Select(x => x.Artist)
          .Distinct();
            var artistDictionary = context.artist.ToDictionary(k => k.title, v => v);
            var relatedArtists = new List<Artist>();
            foreach (var title in relatedArtistTitles)
            {
                Artist temp;
                if (artistDictionary.TryGetValue(title, out temp))
                {
                    relatedArtists.Add(temp);
                }
            }

            return relatedArtists;
        }


    }
}