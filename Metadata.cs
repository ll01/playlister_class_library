﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

namespace Playlister_Class_Library
{
    internal interface IContext_Object
    {
        string Get_Title();

        int Get_ID();
    }

    public abstract class Metadata
    {
        //BitmapImage bitmapImage = new BitmapImage(new Uri("ms-appx://[project-name]/Assets/image.jpg"));
        //public const string Metadata_Id_Path = "ms-appx:///API_IDs.xml";
        //public const string Genre_Path = "ms-appx:///GenreIndex.xml";
        //public const string Artist_Path = "ms-appx:///ArtistIndex.xml";

        protected const string defaultThumbnailPath = "default.jpg";
        protected List<Song> Related_Song = new List<Song>();
        protected List<Artist> Related_Artists = new List<Artist>();
        protected List<Genre> Related_Genres = new List<Genre>();
        protected string Thumbnail_Path;

        abstract public string title {get; set;}


        protected Byte[] _Thumbnail;

        protected Byte[] Thumbnail
        {
            get
            {
                return _Thumbnail;
            }
            set
            {
                _Thumbnail = value;
            }
        }

        public Metadata(string MetadataTitle)
        {
            // var invalid_Characters = "[-|/]+";
            // //TODO: FIX rEX
            // var Name_Clensed = Regex.Replace(MetadataTitle, invalid_Characters, " ");

            // var Name_Capitalised = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Name_Clensed);
            // this.MetadataTitle = Name_Capitalised;
            title = MetadataTitle;
        }

        public Metadata()
        {
        }

        /// <summary>
        /// adds similar artist to related artist list
        /// </summary>
        /// <param name="Artist_To_Add">the artist to be entered in to the list </param>
        /// <returns>whether the artist is already associated </returns>
        public bool Add_Related_Artist(Artist Artist_To_Add)
        {
            if (Related_Artists.Contains(Artist_To_Add))
                return false;
            else
            {
                Related_Artists.Add(Artist_To_Add);
                return true;
            }
        }

        /// <summary>
        /// adds similar genre to related artist list
        /// </summary>
        /// <param name="Genre_To_add">the genre to be entered in to the list</param>
        /// <returns>whether the Genre is already associated</returns>
        public bool Add_Related_Genre(Genre Genre_To_add)
        {
            if (Related_Genres.Contains(Genre_To_add))
                return false;
            else
            {
                Related_Genres.Add(Genre_To_add);
                return true;
            }
        }

        /// <summary>
        /// adds song to relates song list
        /// </summary>
        /// <param name="Song_To_Add">song to add to related song list </param>
        /// <returns></returns>
        public virtual bool Add_Song(Song Song_To_Add)
        {
            if (Related_Song.Contains(Song_To_Add))
                return false;
            else
            {
                Related_Song.Add(Song_To_Add);
                return true;
            }
        }

        public override bool Equals(object obj)
        {
            Metadata Object_To_Test = obj as Metadata;
            if (Object_To_Test.title == title)
                if (Object_To_Test.GetHashCode() == this.GetHashCode())
                    return true;

            return false;
        }

        /// <summary>
        /// gets list of related genre
        /// </summary>
        // /// <returns>list of object genre of related genres </returns>
        abstract public List<Genre> Get_Related_Genres(IMusic_Context context);
       

        abstract public List<Artist> Get_Related_Artists(IMusic_Context context);
      

        /// <summary>
        /// get list of related songs
        /// </summary>
        /// <returns>list of object songs of related songs </returns>
         abstract public List<Song> Get_Related_Songs(IMusic_Context context) ;
       

        public override int GetHashCode()
        {
            int Hash = this.ToString().GetHashCode() * Related_Artists.Count() * Related_Genres.Count() * Related_Song.Count();
            return Hash;
        }

      

        /// <summary>
        /// returns a random associated image relating to the metadata type
        /// </summary>
        /// <returns> System.Drawing.image of a related album art </returns>
        public virtual byte[] Get_Thumbnail(IMusic_Context context)
        {
            int Random_Seed = DateTime.Now.Second;
            Random ImgSelect = new Random(Random_Seed);

            Artist Representing_Artist = null;
            Album Representing_Album = null;
            string str = this.GetType().ToString();
            if (this.GetType().ToString() == "Playlist_Creator.Genre")
                Representing_Artist = Related_Artists[ImgSelect.Next(0, Related_Artists.Count - 1)];
            else
                Representing_Artist = this as Artist;
			var albums = Representing_Artist.Get_Artist_Albums(context);

			Representing_Album = albums[ImgSelect.Next(0, albums.Count - 1)];
            _Thumbnail = Representing_Album.Get_Thumbnail(context);

            return _Thumbnail;
        }

        /// <summary>
        /// findes a picture in the same directory as the song
        /// </summary>
        /// <param name="path">path of a song </param>
        /// <returns>uri of an image (if no image is foun it retuns a defult place holder )</returns>
        public static string Search_Folder_For_Thumbnail(string path)
        {
            String Folder_Path = Path.GetDirectoryName(path);
            string Image_Path = null;
            // search for images in the folder
            String[] Image_File_Extentions = { ".png", ".jpg", ".jpeg", ".bmp" };
            List<string> Potential_Image_Paths = new List<string>();
            foreach (string extention in Image_File_Extentions)
            {
                string[] File_Paths = Directory.GetFiles(Folder_Path, "*" + extention + "*", SearchOption.AllDirectories);
                if (File_Paths != null || File_Paths.Count() == 0)
                    Potential_Image_Paths.AddRange(File_Paths);
            }
            if (Potential_Image_Paths.Count == 0)
            {
                Image_Path = @"\default_Album_Art.png";
            }
            else
            {
                Image_Path = Potential_Image_Paths[0];
            }

            return Image_Path;
        }

        public override string ToString()
        {
            return GetType().Name + " : " + title;
        }
	

	}
}