using Microsoft.EntityFrameworkCore;
using System;

namespace Playlister_Class_Library
{
    public interface IMusic_Context : IDisposable
	{
        void Clear();
        DbSet<Song> song { get; set; }
        DbSet<Genre> genre { get; set; }
        DbSet<Artist> artist { get; set; }
        DbSet<Album> album { get; set; }
        void Save();
    }
    // public class Music_Context : DbContext, IMusic_Context
    // {
    //     public const string Database_Connection_String = " Data source = music_database.db";
    //     private static bool Created = false;
    //     public DbSet<Song> song { get; set; }
    //     public DbSet<Genre> genre { get; set; }

    //     public DbSet<Artist> artist { get; set; }

    //     public DbSet<Album> album { get; set; }


    //     public Music_Context()
    //     {
    //         if (Created == false)
    //         {
    //             Created = true;
    //             Database.EnsureDeleted();
    //             Database.EnsureCreated();
    //         }
    //     }

    //     protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //     {
    //         optionsBuilder.UseSqlite(Database_Connection_String);
    //     }
    //     protected override void OnModelCreating(ModelBuilder modelBuilder)
    //     {
    //         modelBuilder.Entity<Song>()
    //             .HasAlternateKey(x => x.File_Path)
    //             .HasName("AlternateKey_Path");
    //         modelBuilder.Entity<Artist>()
    //           .HasAlternateKey(x => x.title)
    //           .HasName("AlternateKey_Artist_Title");
    //         modelBuilder.Entity<Genre>()
    //           .HasAlternateKey(x => x.title)
    //           .HasName("AlternateKey_Genre_Tile");
    //         modelBuilder.Entity<Album>()
    //           .HasAlternateKey(x => x.Album_Artist)
    //           .HasName("AlternateKey_Album_Artist_Name");

    //     }


    //     public void Clear()
    //     {
    //         song.RemoveRange(song);
    //         genre.RemoveRange(genre);
    //         artist.RemoveRange(artist);
    //         SaveChanges();
    //     }


    //     public void Save()
    //     {
    //         SaveChanges();
    //     }
    // }

}