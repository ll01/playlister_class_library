using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;

namespace Playlister_Class_Library
{
    public class Album : Metadata, IContext_Object
    {
        private string album_Artist;

        [Column]
        public override string title { get ; set; }

        public int id { get; private set; }
        public string Album_Artist { get => album_Artist; set => album_Artist = value; }

        public Album(string Album_Title, string Album_Artist, byte[] Album_Thumbnail)
            : base(Album_Title)
        {
            this.Album_Artist = Album_Artist;
            Thumbnail = Album_Thumbnail;
        }

        public Album(string Album_Title, string Album_Artist)
           : base(Album_Title)
        {
            title = Album_Title;
            this.Album_Artist = Album_Artist;
            // Thumbnail = Defult_Art;
        }

        private Album() : base("nil")
        {
        }

        //// and a song and its associated genre;
        //public void Add_Song_Extract_Metadata(Song Song_To_Add)
        //{
        //    if (Song_To_Add.Get_Artist().Get_Title() == Album_Artist.Get_Title())
        //    {
        //        Song_To_Add.Set_Album(this);
        //    }
        //}

        /// <summary>
        /// returns a random assosiated image relating to the metadata type
        /// </summary>
        /// <returns> System.Drawing.image of a related album art </returns>
        public override Byte[] Get_Thumbnail(IMusic_Context context )
        {
            var Album_Thumbnail = Get_Related_Songs(context)[0].Retrive_Thumbnail();
            if (Album_Thumbnail == null)
            {
                Album_Thumbnail = File.ReadAllBytes(defaultThumbnailPath);
            }
            return Album_Thumbnail;
        }
        
        public string Get_Title()
        {
            return title;
        }

        public int Get_ID()
        {
            return id;
        }

        public override List<Song> Get_Related_Songs(IMusic_Context context)
        {
               return context.song.Where(x => x.Album == title).ToList();
        }

        public override List<Genre> Get_Related_Genres(IMusic_Context context)
        {
            throw new NotImplementedException();
        }

        public override List<Artist> Get_Related_Artists(IMusic_Context context)
        {
            throw new NotImplementedException();
        }
        // public override int GetHashCode() {
        //         return 
        // }
    }
}