﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace Playlister_Class_Library
{
	public class Settings
	{
		public List<string> Monitord_Folders = new List<string>();
		public string Playlist_Folder_Target;
		public int Playlist_Lenth;
		public bool Autoplay_Enabled;

		public static Settings CurrentSettings;

		public static void LoadSettings(string Settings_File_Path)
		{
			try
			{
				using (var stream = new StreamReader(Settings_File_Path))
				{
					var json = stream.ReadToEnd();

					CurrentSettings = JsonConvert.DeserializeObject<Settings>(json);
				}
			} catch (JsonSerializationException e)
			{
				Console.WriteLine("error loading json file using defaults");
				Console.WriteLine("exeption message : {0}", e.Message );
		
				SetDefaultSettings();
			}
			
		}

		public void saveSettings(string Settings_File_Path)
		{
			using (var stream = new StreamWriter(Settings_File_Path))
			{
				var json = JsonConvert.SerializeObject(this);
				stream.Write(json);
				stream.Flush();
			}
		}

		public static void SetDefaultSettings()
		{
			//Defult Settings
			CurrentSettings = new Settings();
			Settings.CurrentSettings.Monitord_Folders.Add(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic));
			string Default_Playlist_Target = Settings.CurrentSettings.Monitord_Folders[0] + "\\playlists";
			if (Directory.Exists(Default_Playlist_Target) == false)
			{
				Directory.CreateDirectory(Default_Playlist_Target);

			}
			Settings.CurrentSettings.Playlist_Folder_Target = Default_Playlist_Target;
			// 30 is defult playlist time
			Settings.CurrentSettings.Playlist_Lenth = 30;
			Settings.CurrentSettings.Autoplay_Enabled = false;
		}

	}
}
